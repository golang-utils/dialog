package dialog

import (
	"fmt"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

const backFooter = "ctrl+b: back • ctrl+c: quit"
const quitFooter = "ctrl+c: quit"

var DefaultGUI GUI

func New(name string, opts ...Option) App {
	s := &app{
		name:     name,
		selected: map[string]interface{}{},
	}

	s.width, s.totalHeight = getTerminalSize()

	for _, opt := range opts {
		opt(s)
	}

	if s.withBackNav {
		s.footer = backFooter
	} else {
		s.footer = quitFooter
	}

	return s
}

var _ App = &app{}

type app struct {
	name   string
	footer string

	screenStack    []ScreenExt
	withBreadcrumb bool
	withHeader     bool
	withFooter     bool
	withBackNav    bool
	noQuit         bool
	totalHeight    int
	width          int

	withGUI GUI

	// maps the screen name to the last given value
	selected map[string]interface{}
}

func (a *app) HasBackNav() bool {
	return a.withBackNav
}

func (a *app) Name() string {
	return a.name
}

func (d *app) Gui() GUI {
	return d.withGUI
}

func (d *app) GetValue(sc Screen) interface{} {
	if v, has := d.selected[sc.ID()]; has {
		return v
	}
	return nil
}

func (d *app) SetValue(sc Screen, value interface{}) {
	d.selected[sc.ID()] = value
}

// clears the selected values
func (d *app) ClearValues() {
	d.selected = map[string]interface{}{}
}

// space available for the body
func (a *app) AvailableLines(sc ScreenExt) int {
	if a.totalHeight == -1 {
		return -1
	}

	lines := a.GetHeader(sc.Header()) + "\n" + a.GetFooter("")
	used := len(strings.Split(lines, "\n"))
	return a.totalHeight - used - 1
}

func (a *app) Run(firstScreen Screen) error {
	first, has := firstScreen.(ScreenExt)

	if !has {
		return fmt.Errorf("screen does not implement the interface ScreenExt")
	}

	if a.withGUI == nil {
		return a.runCLI(first)
	}

	return a.runGUI(first)
}

func (a app) GetBreadcrumb() string {
	if a.withBreadcrumb && len(a.screenStack) > 0 {
		var screennames []string

		for _, sc := range a.screenStack {
			name := sc.Name()
			if name != "" {
				screennames = append(screennames, name)
			}
		}

		if len(screennames) == 0 {
			return ""
		}

		return strings.Join(screennames, " > ")
	}
	return ""
}

func (s app) GetHeader(innerHeader string) string {
	var h string

	if s.withHeader {

		h = s.name

		if h != "" {
			h += "\n"
		}

		bc := s.GetBreadcrumb()

		if bc != "" {
			h += bc + "\n\n"
		}
	}

	if innerHeader != "" {
		return h + "\n" + innerHeader + "\n\n"
	}

	return h + "\n"
}

func (s app) GetFooter(innerFooter string) string {
	f := innerFooter

	if f != "" {
		f += "\n"
	}

	if s.withFooter && s.footer != "" {
		if len(s.screenStack) <= 1 {
			return "\n" + quitFooter + f
		}
		return "\n" + s.footer + f
	}

	return f
}

func (s *app) GoBack() ScreenExt {
	if len(s.screenStack) == 1 {
		return s.lastScreen()
	}

	s.popScreen()

	sc := s.lastScreen()
	if val, has := s.selected[sc.ID()]; has {
		sc.SetValue(val)
	}

	return sc
}

func (a *app) runCLI(firstScreen ScreenExt) error {

	a.screenStack = append(a.screenStack, firstScreen)

	p := tea.NewProgram(newbubbleTeaModel(a, firstScreen), tea.WithAltScreen(), tea.WithMouseCellMotion())
	//p := tea.NewProgram(newbubbleTeaModel(a, firstScreen))

	_, err := p.Run()
	p.ExitAltScreen()
	p.RestoreTerminal()
	//p.Quit()

	return err
}

func (a *app) runGUI(firstScreen ScreenExt) error {
	var s ScreenExt = firstScreen
	var n ScreenExt = nil
	var back bool
	var isFirst = true
	var err error
	for {
		if s == nil {
			return nil
		}

		if !back {
			a.screenStack = append(a.screenStack, s)
		}

		isFirst = len(a.screenStack) <= 1

		n, back, err = s.RunGui(isFirst)

		if err != nil {
			//if err == zenity.ErrCanceled {
			if err == ErrCanceled {
				return nil
			}
			return err
		}

		if back {
			s = a.GoBack()
		} else {
			if n == nil {
				return nil
			}
			s = n
		}

		if val, has := a.selected[s.ID()]; has {
			s.SetValue(val)
		}
	}

	return nil
}

func (s *app) defaultUpdate(msg tea.Msg) (sc ScreenExt, cmd tea.Cmd, handled bool) {
	switch msg := msg.(type) {

	case tea.KeyMsg:

		switch msg.String() {

		// go back
		case "ctrl+b":
			if !s.withBackNav {
				return nil, nil, false
			}

			return s.GoBack(), nil, true

		// These keys should exit the program.
		case "ctrl+c":
			if s.noQuit {
				return nil, nil, false
			}
			return nil, tea.Quit, true
		}
	}

	return nil, nil, false
}

func (s *app) popScreen() {
	switch len(s.screenStack) {
	case 0:
	case 1:
		s.screenStack = nil
	default:
		s.screenStack = s.screenStack[:len(s.screenStack)-1]
	}
}

func (s *app) lastScreen() ScreenExt {
	if len(s.screenStack) == 0 {
		return nil
	}
	return s.screenStack[len(s.screenStack)-1]
}
