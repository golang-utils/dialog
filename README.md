
dialog
======

cross plattform CLI dialogs.


Features
--------

The library is organized in screens.
The following types of screens are available:

- input
- password
- select
- multiselect
- output
- error

- There is also support for input validation.
- When there is too much content for the screen, the pager is enabled.


Example
-------


```go
package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/golang-utils/dialog"
	"gitlab.com/golang-utils/fmtdate"
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	return NewApp().Run()
}

func NewApp() *App {
	var opts []dialog.Option

	opts = append(opts,
		dialog.WithBackNavigation(),
		dialog.WithFooter(),
		dialog.WithHeader(),
		dialog.WithBreadcrumb(),
	)

	a := &App{}

	a.app = dialog.New("Person", opts...)
	return a
}

type Data struct {
	Firstname string
	Lastname  string
	Password  string
	Birthday  string
	Male      bool
	Pets      []string
	Number    string
}

type App struct {
	app  dialog.App
	Data Data
}

func (a *App) Run() error {
	return a.app.Run(a.askforGender())
}

func (a *App) askforGender() dialog.Screen {
	sc := a.app.NewSelect(
		"choose your gender", []string{"female", "male"},

		func(chosen string) dialog.Screen {
			switch chosen {
			case "female":
				a.Data.Male = false
			case "male":
				a.Data.Male = true
			}

			return a.askforFirstname()
		})

	sc.SetOptions(
		dialog.WithID("gender"),
	)

	return sc
}

func (a *App) askforFirstname() dialog.Screen {
	sc := a.app.NewInput(
		"please enter your firstname",

		func(fname string) dialog.Screen {
			a.Data.Firstname = fname

			return a.askforLastname()
		})
	sc.SetOptions(
		dialog.WithID("firstname"),
		dialog.WithValidator(dialog.String),
	)
	return sc
}

func (a *App) askforLastname() dialog.Screen {
	sc := a.app.NewInput(
		"please enter your lastname",

		func(lname string) dialog.Screen {
			a.Data.Lastname = lname

			return a.askforBirthday()
		})

	sc.SetOptions(
		dialog.WithID("lastname"),
		dialog.WithValidator(dialog.String),
	)

	return sc
}

func (a *App) askforBirthday() dialog.Screen {
	sc := a.app.NewInput(
		"please enter your birthday (YYYY-MM-DD)",

		func(bday string) dialog.Screen {
			a.Data.Birthday = bday

			return a.askforPets()
		})

	prefill := fmtdate.Format("YYYY-MM-DD", time.Now())

	sc.SetOptions(
		dialog.WithID("birthday"),
		dialog.WithValidator(dialog.DateTime("YYYY-MM-DD")),
		dialog.WithPrefilled(prefill),
	)

	return sc
}

func (a *App) askforPets() dialog.Screen {
	var options []string

	for i := 1; i <= 40; i++ {
		options = append(options, fmt.Sprintf("Pet %v", i))
	}

	sc := a.app.NewMultiSelect(
		"which pets do you have", options,
		func(pets []string) dialog.Screen {
			a.Data.Pets = pets

			return a.askforNumber()
		})

	sc.SetOptions(
		dialog.WithID("pets"),
		dialog.WithValidator(dialog.And{dialog.Min(3), dialog.Max(5)}),
	)

	return sc
}

func (a *App) askforNumber() dialog.Screen {
	var options []string

	for i := 1; i <= 50; i++ {
		options = append(options, fmt.Sprintf("%v", i))
	}

	sc := a.app.NewSelect(
		"which number do you choose", options,

		func(choosen string) dialog.Screen {
			a.Data.Number = choosen

			return a.askforPassword()
		})

	sc.SetOptions(
		dialog.WithID("number"),
		dialog.WithValidator(dialog.NonEmpty),
	)

	return sc
}

func (a *App) askforPassword() dialog.Screen {
	sc := a.app.NewPassword(
		"please enter your password",
		func(pw string) dialog.Screen {
			a.Data.Password = pw

			return a.showSummary()
		})

	sc.SetOptions(
		dialog.WithID("password"),
		dialog.WithValidator(dialog.String),
	)

	return sc
}

func (a *App) showSummary() dialog.Screen {
	sc := a.app.NewOutput(
		a.summary(),
		func() dialog.Screen {
			return a.byebye()
		})

	return sc
}

func (a *App) byebye() dialog.Screen {
	sc := a.app.NewOutput(
		"Thanks and byebye",
		func() dialog.Screen {
			return dialog.QuitScreen()
		})

	return sc
}

func (a *App) summary() string {
	var g = "f"

	if a.Data.Male {
		g = "m"
	}

	var bd strings.Builder

	for i := 0; i < 5; i++ {
		s := fmt.Sprintf(`
### %v ##
%s %s (%s)
Password: %s
born %s
pets %s
number: %v
	`, i, a.Data.Firstname, a.Data.Lastname, g,
			a.Data.Password,
			a.Data.Birthday,
			strings.Join(a.Data.Pets, ", "),
			a.Data.Number)
		bd.WriteString(s)
	}

	return bd.String()
}

```

also, see example-app

## Documentation

see https://pkg.go.dev/gitlab.com/golang-utils/dialog