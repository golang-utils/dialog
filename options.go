package dialog

type Option func(s *app)

func WithBreadcrumb() Option {
	return func(s *app) {
		s.withBreadcrumb = true
	}
}

func WithHeader() Option {
	return func(s *app) {
		s.withHeader = true
	}
}

func WithFooter() Option {
	return func(s *app) {
		s.withFooter = true
	}
}

func WithBackNavigation() Option {
	return func(s *app) {
		s.withBackNav = true
	}
}

func NoQuit() Option {
	return func(s *app) {
		s.noQuit = true
	}
}

func WithGui(g GUI) Option {
	return func(s *app) {
		if g != nil {
			g.SetApp(s)
			s.withGUI = g
		}
	}
}

func WithDefaultGui() Option {
	return WithGui(DefaultGUI)
}
