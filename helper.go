package dialog

import (
	"github.com/google/uuid"
	"github.com/nsf/termbox-go"
)

func getTerminalSize() (width int, height int) {
	if err := termbox.Init(); err != nil {
		return -1, -1
	}
	w, h := termbox.Size()
	termbox.Close()
	return w, h
}

func newUUID() string {
	return uuid.NewString()
}
