package dialog

import (
	//"fmt"
	"strings"

	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/muesli/reflow/wordwrap"
)

// OutputScreen is a screen just for printing
type outputScreen struct {
	app  appInternal
	body string
	next func() Screen
	//paginator *pager
	config   screenConfig
	viewport viewport.Model
	ready    bool
	width    int
}

var _ Screen = &outputScreen{}

func (a *app) NewOutput(message string, next func() Screen) Screen {
	op := &outputScreen{
		app:   a,
		body:  message,
		next:  next,
		ready: true,
	}
	op.config.UUID = newUUID()

	//panic(fmt.Sprintf("width: %v, height: %v\n", a.width, a.AvailableLines(op)))
	op.width = a.width
	op.viewport = viewport.New(a.width, a.AvailableLines(op)-2)
	//op.viewport.
	//a.totalHeight
	//a.
	//op.viewport = viewport.New(msg.Width, msg.Height)
	//l.viewport.YPosition = headerHeight
	//l.viewport.HighPerformanceRendering = useHighPerformanceRenderer
	op.viewport.HighPerformanceRendering = false
	op.viewport.SetContent(wordwrap.String(op.body, op.width-2))

	//op.paginator = newPager(op)
	return op
}

// NewOutputScreen returns a new OutputScreen
/*
func NewOutputScreen(a App, name, message string, next func() Screen) *OutputScreen {
	op := &OutputScreen{
		app:  a,
		body: message,
		name: name,
		next: next,
		uuid: NewUUID(),
	}

	op.paginator = newPaginator(op)
	return op
}
*/

func (i *outputScreen) SetOptions(opts ...ScreenOption) {
	for _, opt := range opts {
		opt(&i.config)
	}
}

func (i *outputScreen) ID() string {
	return i.config.UUID
}

func (l *outputScreen) SetValue(v interface{}) {

}

func (l *outputScreen) Name() string {
	return l.config.Name
}

func (l *outputScreen) Header() string {
	return ""
}

func (l *outputScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) {
	gui := l.app.Gui()

	if gui == nil {
		panic("gui not defined")
	}

	//l.paginator.needsPager = false
	//l.paginator.setLines()
	//back, err = zentityOutput(l.app, l.body)

	opts := GUIOptions{}
	if l.next == nil {
		opts.OkAsBack = true
	}
	opts.HideBack = isFirst
	opts.Title = l.Name()
	back, err = gui.Output(l.body, opts)

	if back {
		return nil, true, nil
	}

	if err != nil {
		return nil, false, err
	}

	if l.next != nil {
		return l.next().(ScreenExt), false, nil
	}
	return nil, false, nil
}

func (l *outputScreen) AvailableLines() int {
	return l.app.AvailableLines(l)
}

func (l *outputScreen) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	//var cmd tea.Cmd

	var (
		cmd tea.Cmd
	)

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+q", "ctrl+b", "esc", "enter":
			return nil, nil
		}
		/*
			if k := msg.String(); k == "ctrl+c" || k == "q" || k == "esc" {
				return m, tea.Quit
			}
		*/

	case tea.WindowSizeMsg:
		//headerHeight := lipgloss.Height(m.headerView())
		//footerHeight := lipgloss.Height(m.footerView())
		//verticalMarginHeight := headerHeight + footerHeight

		if !l.ready {
			// Since this program is using the full size of the viewport we
			// need to wait until we've received the window dimensions before
			// we can initialize the viewport. The initial dimensions come in
			// quickly, though asynchronously, which is why we wait for them
			// here.
			//l.viewport = viewport.New(msg.Width, msg.Height-verticalMarginHeight)
			l.width = msg.Width
			l.viewport = viewport.New(msg.Width, msg.Height)
			//l.viewport.YPosition = headerHeight
			//l.viewport.HighPerformanceRendering = useHighPerformanceRenderer
			l.viewport.HighPerformanceRendering = false
			l.viewport.SetContent(wordwrap.String(l.body, l.width-2))
			//l.viewport.SetContent(l.body)
			l.ready = true

			// This is only necessary for high performance rendering, which in
			// most cases you won't need.
			//
			// Render the viewport one line below the header.
			//l.viewport.YPosition = headerHeight + 1
		} else {
			l.viewport.Width = msg.Width
			//l.viewport.Height = msg.Height - verticalMarginHeight
			l.viewport.Height = msg.Height
		}
	}

	// Handle keyboard and mouse events in the viewport
	l.viewport, cmd = l.viewport.Update(msg)

	_ = cmd

	//return m, tea.Batch(cmds...)
	return nil, cmd
}

func (l *outputScreen) OnInput(str string) ScreenExt {

	switch str {
	case "up":
	case "down":
	case "enter":
		if l.next != nil {
			return l.next().(ScreenExt)
		}
		return nil
	default:
		//l.paginator.OnInput(str)
	}

	return l
}

func (l *outputScreen) Body() string {
	if !l.ready {
		return "\n  Initializing..."
	}
	return l.viewport.View() + "\n"

	/*
		start, end := l.paginator.GetBounds()

		lines := strings.Split(l.body, "\n")
		b := strings.Join(lines[start:end], "\n")
		b += l.paginator.Body()
		return b + "\n"
	*/
}

func (i *outputScreen) Lines() int {
	return len(strings.Split(wordwrap.String(i.body, i.width-2), "\n"))
}
