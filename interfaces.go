package dialog

/*
TODO:

- Zeilen umbrechen, insbesondere bei output oder ggf. was anderes von den bubbles nehmen

- ggf. für select und multiselect listen sollen einträge, die mit --- beginnen und enden
  als beschriftung nur ausgegeben, aber nicht ausgewählt werden können
  diese sollen beim navigieren mit den pfeiltasten übersprungen werden
  allerdings kann dies zu Problemen in der GUI führen, da müssten diese Einträge
  dann von vornherein rausgenommen werden, was die sache mit dem zählen ziemlich
  komplex werden lassen kann

- für ergänzenden context-text eine eigene screenoption verwenden, die bei der GUI
  nicht zum einsatz kommt

- minimum und max länge für input-feld als validator
- progress (also support by zenity)
- file (also support by zenity)
- date (also support by zenity)
- table (kann durch Zenity supported werden, siehe https://help.gnome.org/users/zenity/stable/list.html.en)

für eine tabelle haben wir keinen support in zenity

- use https://github.com/charmbracelet/bubbles (input field, spinner, textarea, table, progress, paginator
- or https://github.com/erikgeiser/promptkit for some
- maybe also https://github.com/mistakenelf/teacup
- also nice: https://github.com/mritd/bubbles
- paging (find out how many lines are available and page properly)
- autogenerate dialogs for setting config files for the config lib
- change the config lib in a way that makes this dialogs available (and uses the dialog lib)
- multiline-input layout (continue with ctrl+enter, or arrow)
- todo: have a blinking cursor? possible?

*/

type App interface {
	Name() string
	Run(firstScreen Screen) error
	NewPassword(question string, hd func(string) Screen) Screen
	NewInput(question string, hd func(string) Screen) Screen
	NewMultiSelect(question string, choices []string, hd func(s []string) Screen) Screen
	NewOutput(message string, next func() Screen) Screen
	NewSelect(question string, choices []string, hd func(string) Screen) Screen
	NewError(message string) Screen
}

type AppExt interface {
	App
	GetBreadcrumb() string
	GetValue(sc Screen) interface{}
	SetValue(sc Screen, value interface{})
	ClearValues()
	HasBackNav() bool
}

type appInternal interface {
	AppExt
	GetHeader(innerHeader string) string
	GetFooter(innerFooter string) string
	GoBack() ScreenExt
	AvailableLines(sc ScreenExt) int
	Gui() GUI
}

type Screen interface {
	Name() string

	// ID is a unique identifyer for the screen
	// it is set to a UUID by default, but can be set via the WithID option (to get defined IDs)
	// defined IDs are necessary to fill back values when navigating back and forth between screens
	ID() string

	SetOptions(opts ...ScreenOption)
}

type ScreenExt interface {
	Screen
	Body() string
	OnInput(s string) ScreenExt
	Header() string
	RunGui(isFirst bool) (sc ScreenExt, back bool, err error)
	SetValue(interface{})
	Lines() int
	AvailableLines() int
}

type GUIOptions struct {
	Title    string
	Width    uint
	Height   uint
	HideBack bool
	OkAsBack bool
}

type GUI interface {
	Input(msg string, prefilled string, hideText bool, opts GUIOptions) (res string, back bool, err error)
	Select(msg string, items []string, selected string, opts GUIOptions) (res string, back bool, err error)
	MultiSelect(msg string, items []string, preselected []string, opts GUIOptions) (res []string, back bool, err error)
	Output(msg string, opts GUIOptions) (back bool, err error)
	Error(msg string, opts GUIOptions)
	SetApp(a AppExt)
}
