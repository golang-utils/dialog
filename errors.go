package dialog

import (
	"fmt"
)

var ErrCanceled = fmt.Errorf("operation canceled")
