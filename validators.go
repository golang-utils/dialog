package dialog

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/fmtdate"
)

type And []Validator

func (a And) Validate(v interface{}) error {
	for _, vali := range a {
		err := vali.Validate(v)
		if err != nil {
			return err
		}
	}
	return nil
}

type Or []Validator

func (a Or) Validate(v interface{}) error {
	var errs []error
	for _, vali := range a {
		err := vali.Validate(v)
		if err == nil {
			return nil
		}
		errs = append(errs, err)
	}
	return errors.Errors(errs...)
}

type Validator interface {
	// if the value is valid, nil is returned. otherwise we can use the error message
	Validate(v interface{}) error
}

type ValidatorFunc func(interface{}) error

func (fn ValidatorFunc) Validate(v interface{}) error {
	return fn(v)
}

var (
	Any      ValidatorFunc
	Int      ValidatorFunc
	Float    ValidatorFunc
	NonEmpty ValidatorFunc
	String   ValidatorFunc
)

func init() {
	Any = func(v interface{}) error {
		return nil
	}

	Int = func(v interface{}) error {
		switch t := v.(type) {
		case int:
		case int8:
		case int16:
		case int32:
		case int64:
		case uint:
		case uint8:
		case uint16:
		case uint32:
		case uint64:
		case string:
			_, err := strconv.Atoi(t)

			if err != nil {
				return fmt.Errorf("'%s' is no integer", t)
			}

		default:
			return fmt.Errorf("%v is no integer", v)

		}

		return nil
	}

	Float = func(v interface{}) error {
		switch t := v.(type) {
		case float32:
		case float64:
		case string:
			_, err := strconv.ParseFloat(t, 64)

			if err != nil {
				return fmt.Errorf("'%s' is no float", t)
			}

		default:
			return fmt.Errorf("%v is no float", v)

		}

		return nil
	}

	NonEmpty = func(v interface{}) error {
		err := fmt.Errorf("%v is empty", v)
		switch vv := v.(type) {
		case string:
			if vv == "" {
				return err
			}
		case int:
			if vv == -1 {
				return err
			}
		case float32:
			if vv < 0.0 {
				return err
			}
		case float64:
			if vv < 0.0 {
				return err
			}
		default:
			if v == nil {
				return err
			}
		}

		return nil
	}

	String = func(v interface{}) error {
		switch v.(type) {
		case string:
		default:
			return fmt.Errorf("%#v is no string", v)

		}

		return nil
	}
}

type Min int

func (m Min) Validate(v interface{}) error {
	err := fmt.Errorf("%v is smaller than the minimum of %v", v, int(m))
	switch t := v.(type) {
	case int:
		if int(t) < int(m) {
			return err
		}
	case int8:
		if int(t) < int(m) {
			return err
		}
	case int16:
		if int(t) < int(m) {
			return err
		}
	case int32:
		if int(t) < int(m) {
			return err
		}
	case int64:
		if int(t) < int(m) {
			return err
		}
	case uint:
		if int(t) < int(m) {
			return err
		}
	case uint8:
		if int(t) < int(m) {
			return err
		}
	case uint16:
		if int(t) < int(m) {
			return err
		}
	case uint32:
		if int(t) < int(m) {
			return err
		}
	case uint64:
		if int(t) < int(m) {
			return err
		}
	default:
		return fmt.Errorf("%v is no number", v)
	}

	return nil
}

type Max int

func (t Max) Validate(v interface{}) error {
	err := fmt.Errorf("%v is larger than the maximum of %v", v, int(t))
	switch m := v.(type) {
	case int:
		if int(t) < int(m) {
			return err
		}
	case int8:
		if int(t) < int(m) {
			return err
		}
	case int16:
		if int(t) < int(m) {
			return err
		}
	case int32:
		if int(t) < int(m) {
			return err
		}
	case int64:
		if int(t) < int(m) {
			return err
		}
	case uint:
		if int(t) < int(m) {
			return err
		}
	case uint8:
		if int(t) < int(m) {
			return err
		}
	case uint16:
		if int(t) < int(m) {
			return err
		}
	case uint32:
		if int(t) < int(m) {
			return err
		}
	case uint64:
		if int(t) < int(m) {
			return err
		}
	default:
		return fmt.Errorf("%v is no number", v)
	}

	return nil
}

// the string is the format of the expected datetime (excel format)
type DateTime string

func (d DateTime) Validate(v interface{}) error {
	switch t := v.(type) {
	case time.Time:
	case string:
		_, err := fmtdate.Parse(string(d), t)
		if err != nil {
			return fmt.Errorf("%#v is no date/time conforming to the formt %s", v, string(d))
		}
	default:
		return fmt.Errorf("%#v is no date/time conforming to the formt %s", v, string(d))

	}

	return nil

}

type Regexp string

func (d Regexp) Validate(v interface{}) error {
	switch t := v.(type) {
	case string:
		m, err := regexp.MatchString(string(d), t)
		if !m || err != nil {
			return fmt.Errorf("%q is not matching the regular expression %s", v, string(d))
		}
	default:
		return fmt.Errorf("%v is not matching the regular expression %s", v, string(d))

	}

	return nil

}
