package dialog

import (
	"fmt"
	//	"sort"
	"strings"
	"unicode/utf8"
)

// SelectScreen is a screen that shows different options to select one of them
type selectScreen struct {
	app       appInternal
	choices   []string
	header    string
	cursor    int
	handler   func(string) Screen
	paginator *pager
	config    screenConfig
}

var _ Screen = &selectScreen{}

func (s *selectScreen) Lines() int {
	return len(s.choices)
}

func (i *selectScreen) SetOptions(opts ...ScreenOption) {
	for _, opt := range opts {
		opt(&i.config)
	}

	if i.config.Prefilled != nil {
		i.SetValue(i.config.Prefilled)
	}

	// it is needed a second time, since the UUID may have been changed
	// also does the setted value overrule the prefilled
	v := i.app.GetValue(i)

	if v != nil {
		i.SetValue(v)
	}
}

func (i *selectScreen) ID() string {
	return i.config.UUID
}

func (a *app) NewSelect(question string, choices []string, hd func(string) Screen) Screen {
	sl := &selectScreen{
		app:     a,
		header:  question,
		handler: hd,
		choices: choices,
		cursor:  -1,
	}

	sl.config.UUID = newUUID()
	sl.paginator = newPager(sl)

	v := a.GetValue(sl)

	if v != nil {
		sl.SetValue(v)
	}

	return sl
}

func (l *selectScreen) SetValue(v interface{}) {
	sel := v.(string)

	for i, ch := range l.choices {
		if ch == sel {
			l.paginator.SetPageToIndex(i)
			l.cursor = i - l.paginator.start
			return
		}
	}

}

func (l *selectScreen) Name() string {
	return l.config.Name
}

func (l *selectScreen) Header() string {
	return l.header
}

func (l *selectScreen) findWidthForGui() uint {
	//utf8.RuneCountInString()
	var w uint

	for _, choice := range l.choices {
		c := utf8.RuneCountInString(choice)
		if c > int(w) {
			w = uint(c)
		}
	}

	return w
}

func (l *selectScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) {
	gui := l.app.Gui()

	if gui == nil {
		panic("gui not defined")
	}

	l.paginator.needsPager = false
	l.paginator.setLines()
	var res string
	var selected string
	if l.cursor >= 0 && l.cursor <= len(l.choices)-1 {
		selected = l.choices[l.cursor]
	}
	//res, back, err = zentitySelect(l.app, l.header, l.choices, selected)

	opts := GUIOptions{}
	opts.HideBack = isFirst
	opts.Width = l.findWidthForGui()
	opts.Title = l.Name()
	res, back, err = gui.Select(l.header, l.choices, selected, opts)

	if err != nil {
		return nil, false, err
	}

	if back {
		return nil, true, nil
	}

	l.SetValue(res)
	l.saveTostore()

	if errV := l.validate(); errV != nil {
		return l.screenInvalid(errV.Error()), false, nil
	}

	l.saveTostore()

	return l.handler(res).(ScreenExt), false, nil
}

func (l *selectScreen) Body() string {
	var bd strings.Builder

	start, end := l.paginator.GetBounds()

	for i, choice := range l.choices[start:end] {

		// Is the cursor pointing at this choice?
		cursor := " " // no cursor
		if l.cursor == i {
			cursor = ">" // cursor!
		}

		// Render the row
		bd.WriteString(fmt.Sprintf("%s %s\n", cursor, choice))
	}

	bd.WriteString(l.paginator.Body())

	return bd.String()
}

func (l *selectScreen) saveTostore() {
	if l.cursor >= 0 {
		start, _ := l.paginator.GetBounds()
		choice := l.choices[l.cursor+start]
		l.app.SetValue(l, choice)
	}
}

func (l *selectScreen) validate() error {
	if l.config.Validator == nil {
		return nil
	}

	// just for: NotEmpty validation (l.cursor must be > -1
	return l.config.Validator.Validate(l.cursor)
}

func (l *selectScreen) screenInvalid(msg string) ScreenExt {
	sc := l.app.NewError("selection is empty").(ScreenExt)
	sc.SetOptions(WithName("invalid selection"))
	return sc
}

func (l *selectScreen) AvailableLines() int {
	return l.app.AvailableLines(l)
}

func (l *selectScreen) findPrefixed(prefix string) (found []int) {
	for idx, val := range l.choices {
		if strings.HasPrefix(val, prefix) {
			found = append(found, idx)
		}
	}

	return
}

func (l *selectScreen) OnInput(str string) ScreenExt {

	start, end := l.paginator.GetBounds()

	switch str {

	case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
		found := l.findPrefixed(str)
		switch len(found) {
		case 0:
		case 1:
			l.cursor = found[0]
			l.paginator.SetPageToIndex(found[0])
			return l.submit(found[0])
			// jump directly
		default:
			l.paginator.SetPageToIndex(found[0])
			l.cursor = found[0]
		}
	// The "up" and "k" keys move the cursor up
	case "up":
		if l.cursor > 0 {
			l.cursor--
			l.saveTostore()
		}

	// The "down" and "j" keys move the cursor down
	case "down":
		if l.cursor < end-start-1 {
			l.cursor++
			l.saveTostore()
		}

	// The "enter" key and the spacebar (a literal space) toggle
	// the selected state for the item that the cursor is pointing at.
	//case "enter", "right":
	case "enter", " ":
		return l.submit(l.cursor + start)
	default:
		l.paginator.OnInput(str)
	}

	return l
}

func (l *selectScreen) submit(selected int) ScreenExt {
	var choice string

	if errV := l.validate(); errV != nil {
		return l.screenInvalid(errV.Error())
	}
	if l.cursor >= 0 {
		l.saveTostore()
		choice = l.choices[selected]
	}

	return l.handler(choice).(ScreenExt)
}
