package dialog

type quitScreen struct{}

var quituuid = newUUID()

func (q quitScreen) Body() string                                             { return "" }
func (q quitScreen) Header() string                                           { return "" }
func (q quitScreen) Name() string                                             { return "" }
func (q quitScreen) OnInput(string) ScreenExt                                 { return nil }
func (q quitScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) { return nil, false, nil }
func (q quitScreen) SetValue(v interface{})                                   {}
func (q quitScreen) Lines() int                                               { return -1 }
func (q quitScreen) SetOptions(opts ...ScreenOption)                          {}
func (q quitScreen) ID() string {
	return quituuid
}

var _ Screen = &quitScreen{}

// QuitScreen is a Screen that just quits the App
var quit = quitScreen{}

func QuitScreen() Screen {
	return quit
}

func (q quitScreen) AvailableLines() int {
	return 8
}
