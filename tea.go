package dialog

import (
	tea "github.com/charmbracelet/bubbletea"
)

type bubbleTeaModel struct {
	app    *app
	Screen ScreenExt
}

func newbubbleTeaModel(a *app, sc ScreenExt) *bubbleTeaModel {
	bt := &bubbleTeaModel{
		app:    a,
		Screen: sc,
	}
	return bt
}

func (m *bubbleTeaModel) Init() tea.Cmd {
	return nil
}

func (m *bubbleTeaModel) View() string {
	s := m.app.GetHeader(m.Screen.Header())
	s += m.Screen.Body()
	s += m.app.GetFooter("")

	return s
}

type teaUpdater interface {
	Update(msg tea.Msg) (tea.Model, tea.Cmd)
}

func (m *bubbleTeaModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	screen, cmd, handled := m.app.defaultUpdate(msg)

	if handled {
		if screen != nil {
			return newbubbleTeaModel(m.app, screen), cmd
		}
		return m, cmd
	}

	var next ScreenExt

	keymsg, ok := msg.(tea.KeyMsg)

	if ok {
		next = m.Screen.OnInput(keymsg.String())
	}

	cmd = nil

	if tsc, isup := m.Screen.(teaUpdater); isup {
		_, cmd = tsc.Update(msg)
	}

	if next == nil {
		return m, cmd
	}

	if next == quit {
		return m, tea.Quit
	}

	if next != m.Screen {
		m.app.screenStack = append(m.app.screenStack, next)
	}

	if next == m.Screen {
		return m, nil
	}

	return newbubbleTeaModel(m.app, next), nil
}
