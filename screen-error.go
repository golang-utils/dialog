package dialog

import (
	"strings"
)

// ErrorScreen is a screen just for printing an error message
type errorScreen struct {
	app    appInternal
	body   string
	config screenConfig
}

// NewErrorScreen returns a new ErrorScreen
func (a *app) NewError(message string) Screen {
	sc := &errorScreen{
		app:  a,
		body: message,
	}
	sc.config.UUID = newUUID()
	return sc
}

func (i *errorScreen) Lines() int {
	return len(strings.Split(i.body, "\n"))
}

var _ Screen = &errorScreen{}

func (l *errorScreen) SetValue(v interface{}) {

}

func (l *errorScreen) Name() string {
	return l.config.Name
}

func (l *errorScreen) Header() string {
	return ""
}

func (l *errorScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) {
	gui := l.app.Gui()

	if gui == nil {
		panic("gui not defined")
	}

	opts := GUIOptions{}
	opts.Title = l.Name()
	gui.Error(l.body, opts)
	return nil, true, nil
}

func (l *errorScreen) OnInput(s string) ScreenExt {
	return l.app.GoBack()
}

func (l *errorScreen) Body() string {
	return l.body + "\n"
}

func (l *errorScreen) AvailableLines() int {
	return l.app.AvailableLines(l)
}

func (i *errorScreen) SetOptions(opts ...ScreenOption) {
	for _, opt := range opts {
		opt(&i.config)
	}
}

func (i *errorScreen) ID() string {
	return i.config.UUID
}
