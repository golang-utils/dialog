package dialog

import (
	"fmt"
	"unicode/utf8"

	//	"github.com/charmbracelet/bubbles/key"
	//	"github.com/charmbracelet/bubbles/paginator"

	//"os"
	"strings"
)

// MultiSelectScreen is a Screen that allows to choose more than one option of a given number of options
type multiSelectScreen struct {
	app       appInternal
	choices   []string
	selected  map[int]bool
	cursor    int
	header    string
	paginator *pager
	handler   func(s []string) Screen
	config    screenConfig
}

var _ Screen = &multiSelectScreen{}

func (s *multiSelectScreen) Lines() int {
	return len(s.choices)
}

func (i *multiSelectScreen) SetOptions(opts ...ScreenOption) {
	for _, opt := range opts {
		opt(&i.config)
	}

	if i.config.Prefilled != nil {
		i.SetValue(i.config.Prefilled)
	}

	// it is needed a second time, since the UUID may have been changed
	// also does the setted value overrule the prefilled
	v := i.app.GetValue(i)

	if v != nil {
		i.SetValue(v)
	}
}

func (i *multiSelectScreen) ID() string {
	return i.config.UUID
}

func (a *app) NewMultiSelect(question string, choices []string, hd func(s []string) Screen) Screen {
	ms := &multiSelectScreen{
		app:      a,
		choices:  choices,
		header:   question,
		cursor:   -1,
		handler:  hd,
		selected: map[int]bool{},
	}

	ms.config.UUID = newUUID()
	ms.paginator = newPager(ms)

	v := a.GetValue(ms)

	if v != nil {
		ms.SetValue(v)
	}

	return ms
}

/*
// NewMultiSelectScreen returns a new MultiSelectScreen
func NewMultiSelectScreen(a App, name, question string, choices []string, hd func(s []string) Screen) *MultiSelectScreen {
	ms := &MultiSelectScreen{
		app:      a,
		choices:  choices,
		header:   question,
		cursor:   -1,
		handler:  hd,
		selected: map[int]bool{},
		name:     name,
		uuid:     NewUUID(),
	}

	ms.paginator = newPaginator(ms)

	v := a.GetValue(ms)

	if v != nil {
		ms.SetValue(v)
	}

	return ms
}
*/

/*
func (l *MultiSelectScreen) SetSliceBounds(start, end int) {
	l.start = start
	l.end = end
}
*/

func (l *multiSelectScreen) AvailableLines() int {
	return l.app.AvailableLines(l)
}

func (l *multiSelectScreen) SetValue(v interface{}) {
	selected := v.([]string)

	l.cursor = -1

	l.selected = map[int]bool{}

	var hasSetPaginator bool

	for _, sel := range selected {
		for i, ch := range l.choices {
			if sel == ch {
				l.selected[i] = true
				if !hasSetPaginator {
					hasSetPaginator = true
					l.paginator.SetPageToIndex(i)
				}
			}
		}
	}
}

//type Multihandler func(st app, inputs ...string) Screen

func (l *multiSelectScreen) Name() string {
	return l.config.Name
}

func (l *multiSelectScreen) Header() string {
	return l.header
}

func (l *multiSelectScreen) findWidthForGui() uint {
	//utf8.RuneCountInString()
	var w uint

	for _, choice := range l.choices {
		c := utf8.RuneCountInString(choice)
		if c > int(w) {
			w = uint(c)
		}
	}

	return w
}

func (l *multiSelectScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) {
	gui := l.app.Gui()

	if gui == nil {
		panic("gui not defined")
	}

	l.paginator.needsPager = false
	l.paginator.setLines()
	var preselected []string

	for i, is := range l.selected {
		if is {
			preselected = append(preselected, l.choices[i])
		}
	}

	var selected []string
	//selected, back, err = zentityMultiSelect(l.app, l.header, l.choices, preselected)

	opts := GUIOptions{}
	opts.HideBack = isFirst
	opts.Width = l.findWidthForGui()
	opts.Title = l.Name()
	selected, back, err = gui.MultiSelect(l.header, l.choices, preselected, opts)

	if err != nil {
		return nil, false, err
	}

	if back {
		return nil, true, nil
	}

	l.SetValue(selected)
	l.saveToStore()

	if errV := l.validate(); errV != nil {
		return l.screenInvalid(errV.Error()), false, nil
	}

	l.saveToStore()

	if l.handler == nil {
		return nil, false, nil
	}

	return l.handler(selected).(ScreenExt), false, nil
}

func (l *multiSelectScreen) Body() string {
	var bd strings.Builder

	start, end := l.paginator.GetBounds()

	for i, choice := range l.choices[start:end] {

		cursor := " " // no cursor
		if l.cursor == i {
			cursor = ">" // cursor!
		}

		checked := " " // not selected
		if l.selected[i+start] {
			checked = "x" // selected!
		}

		bd.WriteString(fmt.Sprintf("%s [%s] %s\n", cursor, checked, choice))
	}

	bd.WriteString(l.paginator.Body())
	return bd.String()
}

func (l *multiSelectScreen) saveToStore() {
	var selected []string

	for idx, is := range l.selected {
		if is {
			selected = append(selected, l.choices[idx])
		}
	}

	l.app.SetValue(l, selected)
}

func (l *multiSelectScreen) validate() error {
	if l.config.Validator == nil {
		return nil
	}

	var selected []string

	for idx, is := range l.selected {
		if is {
			selected = append(selected, l.choices[idx])
		}
	}

	return l.config.Validator.Validate(len(selected))
}

func (l *multiSelectScreen) screenInvalid(msg string) ScreenExt {
	sc := l.app.NewError(msg).(ScreenExt)
	sc.SetOptions(WithName("invalid selection"))
	return sc
}

func (l *multiSelectScreen) OnInput(str string) ScreenExt {

	start, end := l.paginator.GetBounds()

	switch str {

	// The "up" and "k" keys move the cursor up
	case "up":
		if l.cursor > 0 {
			l.cursor--
		}

	// The "down" and "j" keys move the cursor down
	case "down":
		if l.cursor < end-start-1 {
			l.cursor++
		}

	// The spacebar (a literal space) toggle
	// the selected state for the item that the cursor is pointing at.
	case " ":
		if l.cursor >= 0 {
			l.selected[l.cursor+start] = !l.selected[l.cursor+start]
			l.saveToStore()
		}
	case "enter":
		//case "enter":
		l.saveToStore()
		if errV := l.validate(); errV != nil {
			return l.screenInvalid(errV.Error())
		}

		l.saveToStore()

		if l.handler == nil {
			return nil
		}

		var selected []string

		for idx, is := range l.selected {
			if is {
				selected = append(selected, l.choices[idx])
			}
		}

		return l.handler(selected).(ScreenExt)
	default:
		l.paginator.OnInput(str)
	}
	return l
}
