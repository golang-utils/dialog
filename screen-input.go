package dialog

import (
	//	"strings"
	"unicode/utf8"

	//	"github.com/ahmetalpbalkan/go-cursor"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
)

// InputScreen is a Screen that allows user input
type inputScreen struct {
	app      appInternal
	answer   string
	password bool
	//name      string
	header string
	//validator Validator
	handler   func(string) Screen
	textInput textinput.Model
	config    screenConfig
}

func (i *inputScreen) Lines() int {
	return 1
}

func (i *inputScreen) SetOptions(opts ...ScreenOption) {
	for _, opt := range opts {
		opt(&i.config)
	}

	if i.config.CharLimit > 0 {
		i.textInput.CharLimit = i.config.CharLimit
	}

	if i.config.Prefilled != nil {
		i.SetValue(i.config.Prefilled)
	}

	// it is needed a second time, since the UUID may have been changed
	// also does the setted value overrule the prefilled
	v := i.app.GetValue(i)

	if v != nil {
		i.SetValue(v)
	}
}

var _ Screen = &inputScreen{}

// NewPasswordScreen returns a new InputScreen where the input is not printed
/*
func NewPasswordScreen(a App, name, question string, validator Validator, hd func(string) Screen) *InputScreen {
	ic := NewInputScreen(a, name, question, validator, hd)
	ic.password = true
	ic.textInput.EchoMode = textinput.EchoPassword
	ic.textInput.EchoCharacter = '•'
	return ic
}
*/

func (a *app) NewPassword(question string, hd func(string) Screen) Screen {
	ic := a.newInput(question, hd)
	ic.password = true
	ic.textInput.EchoMode = textinput.EchoPassword
	ic.textInput.EchoCharacter = '•'
	return ic
}

func (i *inputScreen) ID() string {
	return i.config.UUID
}

func (a *app) NewInput(question string, hd func(string) Screen) Screen {
	return a.newInput(question, hd)
}

func (a *app) newInput(question string, hd func(string) Screen) *inputScreen {
	ip := &inputScreen{
		app:     a,
		header:  question,
		handler: hd,
		answer:  "",
	}
	ip.config.UUID = newUUID()

	ti := textinput.New()
	//ti.Placeholder = "Pikachu"
	ti.Focus()
	//ti.CharLimit = 156
	//ti.Width = 20

	ip.textInput = ti

	v := a.GetValue(ip)

	if v != nil {
		ip.SetValue(v)
	}

	return ip
}

func (l *inputScreen) SetValue(v interface{}) {
	l.answer = v.(string)
	l.textInput.SetValue(l.answer)
}

func (l *inputScreen) Name() string {
	return l.config.Name
}

func (l *inputScreen) Header() string {
	return l.header
}

func (l *inputScreen) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	//var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyEnter, tea.KeyCtrlC, tea.KeyEsc:
			return nil, nil
		}

	// We handle errors just like any other message
	case error:
		//m.err = msg
		return nil, nil
	}

	l.textInput, _ = l.textInput.Update(msg)
	return nil, nil
}

func (l *inputScreen) AvailableLines() int {
	return l.app.AvailableLines(l)
}

func (l *inputScreen) Body() string {
	return l.textInput.View() + "\n"
}

func (l *inputScreen) validate() error {
	if l.config.Validator == nil {
		return nil
	}

	return l.config.Validator.Validate(l.answer)
}

func (l *inputScreen) screenInvalid(msg string) ScreenExt {
	sc := l.app.NewError(msg).(*errorScreen)
	sc.SetOptions(WithName("invalid value"))
	return sc
}

func (l *inputScreen) RunGui(isFirst bool) (sc ScreenExt, back bool, err error) {
	//res, back, err := zenityInput(l.app, l.header, l.answer, l.password)

	gui := l.app.Gui()

	if gui == nil {
		panic("gui not defined")
	}

	opts := GUIOptions{}
	opts.HideBack = isFirst
	opts.Title = l.Name()
	res, back, err := gui.Input(l.header, l.answer, l.password, opts)

	if err != nil {
		return nil, false, err
	}

	if back {
		return nil, true, nil
	}

	l.answer = res
	l.saveToStore()

	if errV := l.validate(); errV != nil {
		return l.screenInvalid(errV.Error()), false, nil
	}

	l.saveToStore()
	if l.handler == nil {
		return nil, false, nil
	}

	return l.handler(l.answer).(ScreenExt), false, nil
}

func (l *inputScreen) saveToStore() {
	l.app.SetValue(l, l.answer)
}

func (l *inputScreen) OnInput(str string) ScreenExt {
	switch str {

	case "up", "down":
	case "pgup", "pgdown":
	case "enter":
		l.saveToStore()

		if errV := l.validate(); errV != nil {
			return l.screenInvalid(errV.Error())
		}

		if l.handler == nil {
			return nil
		}

		return l.handler(l.answer).(ScreenExt)

	case "backspace":
		if len(l.answer) > 0 {
			l.answer = l.answer[:len(l.answer)-1]
			l.saveToStore()
		}

	default:
		// ignore all special keys
		if utf8.RuneCountInString(str) == 1 {
			l.answer = l.answer + str
			l.saveToStore()
		}
	}

	return l
}
