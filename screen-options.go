package dialog

type ScreenOption func(*screenConfig)

// ID is a unique identifyer for the screen
// it is set to a UUID by default, but can be set via the WithID option (to get defined IDs)
// defined IDs are necessary to fill back values when navigating back and forth between screens
func WithID(s string) ScreenOption {
	return func(conf *screenConfig) {
		conf.UUID = s
	}
}

// WithPrefilled prefills the value of a screen
func WithPrefilled(val interface{}) ScreenOption {
	return func(conf *screenConfig) {
		conf.Prefilled = val
	}
}

// WithValidator sets a validator for a (input) screen
func WithValidator(v Validator) ScreenOption {
	return func(conf *screenConfig) {
		conf.Validator = v
	}
}

// WithName sets the name of the screen
func WithName(n string) ScreenOption {
	return func(conf *screenConfig) {
		conf.Name = n
	}
}

// WithCharLimit sets the charlimit for an (input) screen
func WithCharLimit(n int) ScreenOption {
	return func(conf *screenConfig) {
		conf.CharLimit = n
	}
}

type screenConfig struct {
	Prefilled interface{}
	Validator Validator
	Name      string // when name is optional, only setted names should appear in the breadcrumb
	CharLimit int
	UUID      string
}

/*
for password and input, it would be

sc := a.NewInput(question string, handler func(string) Screen)
sc.SetOptions(opts ...ScreenOption)

where valid options would be
(x) Prefilled
(x) Validator (could be String, Integer, Float, DateTime, NonEmpty)
(x) Name
(x) CharLimit

for multiselect, it would be

a.NewMultiSelect(question string, choices []string, handler func(idx []int) Screen)

where valid options would be
(x) Prefilled
(x) Validator (could be Min, Max)
(x) Name

for select, it would be

a.NewSelect(question string, choices []string, handler func(idx int) Screen)

where valid options would be
(x) Prefilled
(x) Validator (could be just NonEmpty)
(x) Name

for output, it would be

a.NewOutput(message string, next func() Screen)

where valid options would be
(x) Name


invalid options are ignored

bei select und multiselect muss bei prefilled der index erscheinen (wie überhaupt bei der auswahl, der handler und validator
alle bekommen nur die indizes, idx = -1 bedeutet; nichts ausgewählt (bei select)
*/
