package dialog

import (
	//"fmt"

	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/paginator"
	"github.com/charmbracelet/lipgloss"
)

type pager struct {
	needsPager bool
	paginator  paginator.Model
	Screen     ScreenExt
	start      int
	end        int
	lines      int
}

func newPager(sc ScreenExt) (pg *pager) {

	pg = &pager{Screen: sc}

	pg.start = -1
	pg.end = -1

	//itemsPerPage := 10

	itemsPerPage := sc.AvailableLines()

	//panic(fmt.Sprintf("screen: %q itemsperpage: %v\n", sc.Name(), itemsPerPage))

	if itemsPerPage < 5 {
		itemsPerPage = 5
	}

	pg.lines = pg.Screen.Lines()

	if pg.lines > itemsPerPage {
		pg.needsPager = true

		p := paginator.New()
		p.Type = paginator.Dots
		p.PerPage = itemsPerPage
		p.ActiveDot = lipgloss.NewStyle().Foreground(lipgloss.AdaptiveColor{Light: "235", Dark: "252"}).Render("•")
		p.InactiveDot = lipgloss.NewStyle().Foreground(lipgloss.AdaptiveColor{Light: "250", Dark: "238"}).Render("•")
		p.SetTotalPages(pg.lines)

		p.KeyMap = paginator.KeyMap{
			PrevPage: key.NewBinding(key.WithKeys("pgup"), key.WithKeys("left")),
			NextPage: key.NewBinding(key.WithKeys("pgdown"), key.WithKeys("right")),
		}

		pg.paginator = p
	}

	return
}

func (l *pager) setLines() {
	if l.needsPager {
		l.start, l.end = l.paginator.GetSliceBounds(l.lines)
		return
	}
	l.start = 0
	l.end = l.lines
}

func (l *pager) GetBounds() (start, end int) {
	l.setLines()
	return l.start, l.end
}

func (l *pager) Body() string {
	//l.setLines()

	if l.needsPager {
		return "\n" + l.paginator.View() + "\n←/→ page"
	}
	return ""
}

func (l *pager) SetPageToIndex(idx int) {
	l.setLines()
	if l.needsPager {
		//panic(fmt.Sprintf("SetPageToIndex: %v", idx))
		for idx > l.end {
			l.paginator.NextPage()
			l.setLines()
		}

		for idx < l.start {
			l.paginator.PrevPage()
			l.setLines()
		}
	}
}

func (l *pager) OnInput(str string) {
	//l.setLines()

	switch str {

	case "pgup", "left":
		if l.needsPager {
			l.paginator.PrevPage()
			l.start, l.end = l.paginator.GetSliceBounds(l.lines)
		}
	case "pgdown", "right":
		if l.needsPager {
			l.paginator.NextPage()
			l.start, l.end = l.paginator.GetSliceBounds(l.lines)
		}
	}
}
